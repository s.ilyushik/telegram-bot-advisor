import os

from aiogram import types, Router, F
from aiogram.filters import CommandStart
import nest_asyncio
from g4f.client import Client as GPTClient

from answers.main_answers import INTRO
from core.config import BOT
from filters.chat_types import ChatTypeFilter

nest_asyncio.apply()


gpt_client = GPTClient()
user_private_router = Router()
user_private_router_filter_args = ["private", "group", "supergroup"]
user_private_router.message.filter(
    ChatTypeFilter(user_private_router_filter_args)
)

BOT_TOKEN = os.getenv('BOT_TOKEN')
bot = BOT


@user_private_router.message(CommandStart())
async def echo_start_message(message: types.Message) -> None:
    """
    Start cmd echo method.
    """

    text = "Привет, я Сереженька!"
    await message.reply(text=text)


# TODO просто пончик
@user_private_router.message(F.text.lower().contains("пончик"))
async def echo_donut_message(message: types.Message) -> None:
    """
    Get a donut method.
    """

    text = "Пончик"
    await message.reply(text=text)


@user_private_router.message()
async def echo_bot_personal_message(message: types.Message) -> None:
    """
    Main echo bot personal message method.
    """

    bot_info = await bot.get_me()
    bot_username = f"@{bot_info.username}"

    if bot_username in message.text or message.chat.type == "private":
        await bot.send_message(
            chat_id=message.chat.id,
            text="Сереженьке нужно подумать!..."
        )

        intro = INTRO
        clear_text = intro + message.text.replace(bot_username, "")
        response = gpt_client.chat.completions.create(
            client=gpt_client,
            model="gpt-4",
            messages=[{"role": "user", "content": clear_text}],
        )
        answer = response.choices[0].message.content

        await message.reply(text=answer)
