from aiogram import types, Router, F
from aiogram.filters import Command, or_f

from filters.chat_types import ChatTypeFilter

user_help_service_router = Router()
user_help_service_filter_args = ["private", "group", "supergroup"]
user_help_service_router.message.filter(ChatTypeFilter(user_help_service_filter_args))


@user_help_service_router.message(
    or_f(
        Command("help"),
        (F.text.lower() == "help")
    )
)
async def main_help_message(message: types.Message) -> None:
    """
    Get help method.
    """

    text = "Ща поможем..."
    # TODO
    # Версия проекта и о проекте
    # Менюшка с командами по запросу
    await message.answer(text=text)
