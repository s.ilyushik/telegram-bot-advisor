from aiogram.types import BotCommand


PRIVATE_COMMANDS = [
    BotCommand(command='menu', description='Посмотреть меню'),
    BotCommand(command='about', description='Обо мне'),
]
