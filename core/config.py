import os

from aiogram import Router, Bot
from dotenv import load_dotenv, find_dotenv


load_dotenv(find_dotenv())


user_private_router = Router()
BOT_TOKEN = os.getenv("BOT_TOKEN")
BOT = Bot(token=BOT_TOKEN)


ALLOWED_UPDATES = ["message," "edited_message"]
