import asyncio
import logging

from aiogram import Dispatcher
from aiogram.types import BotCommandScopeAllPrivateChats

from core.bot_commands_list import PRIVATE_COMMANDS
from core.config import BOT, ALLOWED_UPDATES
from handlers.user_help_service_handler import user_help_service_router
from handlers.user_private_handler import user_private_router


bot = BOT
dp = Dispatcher()

dp.include_routers(user_help_service_router, user_private_router,)


async def main() -> None:
    """
    Main app method.
    """

    logging.basicConfig(level=logging.INFO)
    await bot.delete_webhook(drop_pending_updates=True)
    await bot.set_my_commands(
        commands=PRIVATE_COMMANDS,
        scope=BotCommandScopeAllPrivateChats()
    )
    await dp.start_polling(bot, allowed_updates=ALLOWED_UPDATES)


if __name__ == "__main__":
    asyncio.run(main())
